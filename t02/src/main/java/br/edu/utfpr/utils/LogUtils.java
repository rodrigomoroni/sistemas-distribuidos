package br.edu.utfpr.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Classe utilitária para logar as informações ou exceções geradas no sistema.
 * 
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public class LogUtils {

    /**
     * Construtor com exceção informando que esta classe é utilitaria.
     */
    public LogUtils() {
        throw new IllegalStateException("Classe utilitaria");
    }

    
    /**
     * Logar mensagem em nivel INFO.
     * @param o Objeto onde esta ocorrendo ação
     * @param msg Mensagem de informação.
     * @param args Argumentos da mensagem.
     */
    public static void info(Object o, String msg, Object... args) {
        Logger.getGlobal().log(Level.INFO, String.format(msg, args));
    }
    
    /**
     * Logar mensagem e/ou exceção em nivel ERROR.
     * @param o Objeto onde esta ocorrendo ação
     * @param ex Exceção ocorrida.
     * @param msg Mensagem de informação.
     */
    public static void error(Object o,Throwable ex, String msg) {
        Logger.getGlobal().log(Level.SEVERE, msg,ex);
    }

    /**
     * Logar mensagem e/ou exceção em nivel DEBUG.
     * @param o Objeto onde esta ocorrendo ação
     * @param ex Exceção ocorrida.
     * @param msg Mensagem de informação.
     * @param args Argumentos da mensagem.
     */
    public static void debug(Object o,Throwable ex, String msg, Object... args) {
        Logger.getGlobal().log(Level.FINE, String.format(msg, args),ex);
    }
 
    /**
     * Desliga acompanhamento por log.
     */
    public static void logOff(){
        Logger.getGlobal().setLevel(Level.OFF);
    }
    
    /**
     * Aciona acompanhamento por log em maior nivel.
     */
    public static void logon(){
        // log ativado em maior nivel (8).
        logon(8);
    }
    
    /**
     * Log ativado conforme nivel
     * @param level Nivel de 0 a 7.
     */
    public static void logon(int level){
        
        // nivel padrão acima do 7.
        String levelProp = "ALL";
        
        // estipulad nivel.
        if(level < 7){
            levelProp = String.format("%d00", level + (level<3?3:4));
        }
        // Aplica nivel.
        Logger.getGlobal().setLevel(Level.parse(levelProp));
    }
    
}
