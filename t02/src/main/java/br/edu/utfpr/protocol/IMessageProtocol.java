package br.edu.utfpr.protocol;

/**
 * Diretriz básica do protocolo de mensagens.
 * 
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public interface IMessageProtocol {
    
    /**
     * Realiza montagem da mensagem através dos bytes informados.
     * @param data Arrays de bytes para montagem da mensagem.
     */
     void parse(byte[] data);
    
    /**
     * Realiza transformação da mensagem em array de bytes para transmissão.
     * @return Arrays de bytes para transmissão.
     */
    byte[] serialize();
    
}
