/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.protocol;

import br.edu.utfpr.utils.LogUtils;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import java.util.Arrays;
import sun.security.rsa.RSAPublicKeyImpl;

/**
 * 
 * Implementação do protocolo de mensagem.
 * 
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public class MessageProtocol implements IMessageProtocol {

    /** Identificação do remetente. */
    private int owner;

    /** Tipo de mensagem. */
    private TypeMessage typeMessage;

    /** Conteúdo da mensagem. */
    private Object content;

    /**
     * Define tipo de mensagem.
     * <ul>
     *      <li>HELLO - Entrada no grupo com envio de chave pública.</li>
     *      <li>WORLD - Resposta do grupo  a entrada de novo membro com envio de chave pública.</li>    
     *      <li>GET_CLOCK - Requisição de horario do master a algum membro.</li>
     *      <li>IM_LIVE - Disponibilidade do master.</li>
     *      <li>ELECTION - Abertura de eleição.</li>
     *      <li>VOTE - Votação para novo master com envio de id do escolhido.</li>
     *      <li>TIME - Tempo entre membros com envio de tempo em timestamp.</li>
     *      <li>CRYPTO - Ajuste do tempo dos membros com assinatura do master.</li>
     *      <li>DEATH - Informa que determinado mebro esta morto com envio do id do membro. </li>
     * </ul>
     */
    public enum TypeMessage {
        
        /** Entrada no grupo com envio de chave pública. */
        HELLO,
        /** Resposta do grupo  a entrada de novo membro com envio de chave pública. */
        WORLD,
        /** Requisição de horario do master a algum membro. */
        GET_CLOCK,
        /** Disponibilidade do master. */ 
        IM_LIVE,
        /** Abertura de eleição. */
        ELECTION,
        /** Votação para novo master com envio de id do escolhido. */
        VOTE,
        /** Tempo entre membros com envio de tempo em timestamp. */
        TIME,
        /** Ajuste do tempo dos membros com assinatura do master. */
        CRYPTO,
        /** Informa que determinado membro esta morto com envio do id do membro. */
        DEATH

    }
    
    /**
     * Construtor padrão.
     */
    public MessageProtocol() {
    }

    /**
     * Construtor para preenchimento rápido.
     * @param owner Identificação do remetente.
     * @param typMessage Tipo de mensagem.
     * @param content Conteúdo da mensagem.
     */
    public MessageProtocol(int owner, TypeMessage typMessage, Object content) {
        this.owner = owner;
        this.typeMessage = typMessage;
        this.content = content;
    }

    @Override
    public void parse(byte[] data) {

        // Armazena os dados em buffer.
        ByteBuffer buffer = ByteBuffer.wrap(data);

        // Obtém tipo e remetente da mensagem.
        this.typeMessage = TypeMessage.values()[buffer.get()];
        this.owner = buffer.getInt();
        
        try {
            // Conteúdo conforme tipo de mensagem.
            switch (typeMessage) {
                case HELLO:
                case WORLD:
                    // Recupera chave publica.
                    this.content = new RSAPublicKeyImpl(getBytes(buffer));
                    break;
                case TIME:
                    // Recupera long (data/hora).
                    this.content = buffer.getLong();
                    break;
                case CRYPTO:
                    // Recupera array de bytes.
                    this.content = getBytes(buffer);
                    break;
                default:
                    // Recupera inteiro.
                    this.content = buffer.getInt();
                    break;
            }
        } catch (InvalidKeyException e) {
            LogUtils.error(this, e, "Erro ao parsear mensagem.");
        }

    }

    @Override
    public byte[] serialize() {

        // Aloca buffer com tamanhao maximo esperado.
        ByteBuffer buffer = ByteBuffer.allocate(512);

        // Insere tipo de mensagem e remetente no buffer.
        buffer.put((byte) typeMessage.ordinal());
        buffer.putInt(owner);

        
        switch (typeMessage) {
                case HELLO:
                case WORLD:
                    // Insere chave publica.
                    insertByteArray(buffer, ((PublicKey) content).getEncoded());
                    break;
                case TIME:
                    // Insere long (data/hora).
                    buffer.putLong((long) content);
                    break;
                case CRYPTO:
                    // Insere array de bytes.
                    insertByteArray(buffer, (byte[])content);
                    break;
                default:
                    // Insere inteiro.
                    buffer.putInt((Integer)content);
                    break;
            }

        // Remove espaço desnecesário do buffer antes do envio.
        return Arrays.copyOf(buffer.array(), buffer.position());
    }

    /**
     * Insere vetor de bytes no buffer
     * @param buffer Buffer de bytes.
     * @param array vetor de bytes.
     */
    private void insertByteArray(ByteBuffer buffer, byte[] array){
        
        // Insere tamanho do vetor.
        buffer.putShort((short) array.length);
        // Insere vetor de bytes.
        buffer.put(array);
    }
    
    /**
     * Obtém bytes com tamanho informado posteriormente, usado em objetos não-primitivos.
     * @param buffer Buffer com os dados.
     * @return array de bytes do conteudo.
     */
    public static byte[] getBytes(ByteBuffer buffer){
        
        // Tamanho informado para o objeto.
        int len = (int) buffer.getShort();
        // Array para armazenar os bytes do conteúdo.
        byte [] contentBytes = new byte[len];
        // Transfere bytes do buffer para o array.
        buffer.get(contentBytes, 0, len);
        
        return contentBytes;
    }

    @Override
    public String toString() {
        return String.format("{type: %s, owner: %d, content: %s}",typeMessage.name(),owner,content.toString());
    }

    /**
     * Obtém Identificação do remetente.
     * @return identificação do remetente.
     */
    public int getOwner() {
        return owner;
    }

    /**
     * Define identificação do remetente.
     * @param owner Identificação do remetente.
     */
    public void setOwner(int owner) {
        this.owner = owner;
    }

    
    /**
     * Obtém tipo de mensagem.
     * @return Tipo de mensagem.
     */
    public TypeMessage getTypMessage() {
        return typeMessage;
    }

    /**
     * Define tipo de mensagem.
     * @param typMessage Tipo de mensagem.
     */
    public void setTypMessage(TypeMessage typMessage) {
        this.typeMessage = typMessage;
    }

    /**
     * Obtém conteúdo da mensagem.
     * @return Conteúdo da mensagem.
     */
    public Object getContent() {
        return content;
    }

    /**
     * Define conteúdo da mensagem.
     * @param content Conteúdo da mensagem.
     */
    public void setContent(Object content) {
        this.content = content;
    }

}
