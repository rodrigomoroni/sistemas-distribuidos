/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr;

import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

@Deprecated
public class MulticastPeer {

        
    public void inicia(){
        MulticastSocket s = null;
        String host = "228.5.6.10";
        String reply = "0";
        
        try (Scanner scanner = new Scanner(System.in)) {            
            InetAddress group = InetAddress.getByName(host);
            s = new MulticastSocket(6789);
            s.joinGroup(group); //se junta ao multicast 
            Tread thread = new Tread(s);
            thread.start();
            while(!reply.equals("-1")){
                System.out.println("Mensagem:");
                reply = scanner.nextLine(); //espera do teclado
                DatagramPacket messageOut = new DatagramPacket(reply.getBytes(), reply.getBytes().length, group, 6789);
                s.send(messageOut);                
            }
            thread.setStop(true);
            s.leaveGroup(group);

        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }
    
    @Deprecated
    private class Tread extends Thread {

    private MulticastSocket s;
    private boolean stop = false;
    
    public Tread(MulticastSocket s) {
        this.s = s;
    }
    
    public void setStop(boolean value){
        this.stop = value;
    }
    
    @Override
    public void run() {
        while (!stop){
            byte[] buffer = new byte[1000]; //cria buffer
            DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
            try {
                s.receive(messageIn);
            } catch (IOException ex) {
                Logger.getLogger(Tread.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Received:" + new String(messageIn.getData()));
        }
    }
}

    
}

