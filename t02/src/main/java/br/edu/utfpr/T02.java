/*
 * Autores: Erik J. Almeida () e Rodrigo G. Moroni. de Souza (1795481)
 * Trabalho 02
 * Sincronização de Hora
 ********************************************************************
  ___   _      ___   _      ___   _      ___   _      ___   _
 [(_)] |=|    [(_)] |=|    [(_)] |=|    [(_)] |=|    [(_)] |=|
  '-`  |_|     '-`  |_|     '-`  |_|     '-`  |_|     '-`  |_|
 /mmm/  /     /mmm/  /     /mmm/  /     /mmm/  /     /mmm/  /
       |____________|____________|____________|____________|
                             |            |            |
                         ___  \_      ___  \_      ___  \_
                        [(_)] |=|    [(_)] |=|    [(_)] |=|
                         '-`  |_|     '-`  |_|     '-`  |_|
                        /mmm/        /mmm/        /mmm/
******************************************************************** 
Atividade 1 - Troca de Chaves e Escolha do Mestre: 
    Cada host envia mensagem Multicast para avisar sua entrada no grupo
    enviando sua chave (HELLO) e em seguida recebe como resposta as cha
    ves dos demais membros (WORLD). Caso não haja master ativo, ou seja,
    se os hosts não obtiverem respostas do master (ServiceLive) será fei
    ta uma eleição, na qual cada host irá enviar o menor id encontrado n
    sua lista de id para o multicast. Este número será o candidato 'esco
    lhido' por cada host, a partir das respostas via multicast será calc
    ulado o vencedor da eleição. A partir deste ponto todos hosts slaves
    devem ter como master o 'vencedor', a situação da eleição só ocorrerá
    novamente caso não haja mais disponibilidade do master. Os novos inte
    grantes do grupo Multicast irão trocar chaves (HELLO WORLD) e irão re
    ceber (IM ALIVE) do master atual.

Atividade 2 - ServiceLive:
    Quando o Master do multicast estiver determinado, este deverá irá
    abrir uma thread que enviará uma mensagem para os Slave's dizendo
    que "i'm still alive" em intervalos de tempo. A tarefa dos slaves
    é verificar o recebimento dessas mensagens no tempo combinado.

Atividade 3:
    Aplicação do Algoritmo de Berkeley, aqui a comunicação passa a ser
    unicast onde o master entra em contato com cada slave para verifi-
    car seu horário (GET_CLOCK) e envia o ajuste (CRYPTO), com assina-
    tura do master, baseado no algoritmo. É possível passar como arg a
    opção de simulação de erro, neste caso o host slave, de forma pseu
    do-aleatória configura sua hora atual 'errada' para simular o RTT.
    O atributo referente ao tempo (ou hora atual) está em milisegundos
    e portanto seu ajuste. exemplo de args para simular erro: 5 simu

 */
package br.edu.utfpr;

import br.edu.utfpr.host.MultipleHost;
import br.edu.utfpr.host.UniqueHost;
import br.edu.utfpr.host.AbstractHost;
import br.edu.utfpr.utils.LogUtils;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 * Inicializador da aplicação e hosts.
 *
 * @author Erik J Almeida e Rodrigo G Moroni.
 */
public class T02 {

    /**
     * Numero de hosts para esse servidor.
     */
    private static int numbersProcess = 1;

    /**
     * hosts iniciados pela aplicação.
     */
    public static final Map<Integer, AbstractHost> HOSTS = new HashMap<>();

    /**
     * Flag para simulação de erro de envio.
     */
    private static volatile boolean simu = false;

    /**
     * Inicializador da aplicação
     *
     * @param args numero de processos.
     */
    public static void main(String[] args) {

        // Verifica se numero de processos foi informado no args.
        if (args.length > 0) {
            if (args[0].matches("[\\d]{1,2}")) {
                numbersProcess = Math.max(1, Integer.parseInt(args[0]));
            }
            simu = args.length > 1 && "simu".equalsIgnoreCase(args[args.length - 1]);
        }

        int num_started = 1;
        // caso tenha apenas um processo, será UniqueHost.
        if (numbersProcess == 1) {
            try {
                // Converte ip local em id (integer).
                Integer id = ByteBuffer.wrap(InetAddress.getLocalHost().getAddress()).getInt();
                // Iniciad novo host com o identificador informado.
                AbstractHost a = new UniqueHost(id);
                // Insere no mapeamento de hosts.
                HOSTS.put(id, a);
                // Inicia thread de execução do host.
                new Thread(a).start();
            } catch (UnknownHostException e) {
                LogUtils.error(new T02(), e, "Erro ao chamar host.");
            }
        } else {

            // instância os multiple Hosts.
            for (; num_started <= numbersProcess; num_started++) {
                start(num_started);
            }
        }

        console(num_started);
    }

    /**
     * Console de instruções e administração dos hosts.
     *
     * @param last_id Ultimo id utilizado.
     */
    private static void console(int last_id) {

        // Comando para ser executado.
        String command = "0";

        // mantém o console ativo.
        while (!"exit".equalsIgnoreCase(command)) {
            // obtém comando do "console".
            command = JOptionPane.showInputDialog("@root > ");

            // checa se comando não é nulo.
            if (command != null) {
                // Finaliza host conforme id informado.
                Matcher m = Pattern.compile("^kill( ?-?\\d*)$").matcher(command);
                if (m.find()) {
                    kill(m.group(1).trim());
                    // Para o acompanhamento por log
                } else if ("start".equalsIgnoreCase(command) && numbersProcess != 1) {
                    start(last_id++);
                } else if ("stop".equalsIgnoreCase(command)) {
                    LogUtils.logOff();
                    // Define exibição de log, e nivel.(0-7)
                } else if (command.matches("^show ?\\d?$")) {
                    showLog(command);
                }
            }

        }
        // finaliza todos os hosts.
        HOSTS.values().forEach(e -> e.setRunning(false));
    }

    /**
     * Comando para matar hosts.
     *
     * @param idStr Comando fornecido;
     */
    private static void kill(String idStr) {

        if (idStr.isEmpty()) {
            HOSTS.values().forEach(e -> e.setRunning(false));
        } else {
            int id = Integer.parseInt(idStr);
            AbstractHost a = HOSTS.get(id);
            if (a != null) {
                a.setRunning(false);
            }
        }
    }

    /**
     * Comando para mostrar o log.
     *
     * @param command Comando fornecido.
     */
    private static void showLog(String command) {
        String num = command.replaceAll("\\D", "");
        // se informado nivel, altera nivel do log, senão mostra tudo.
        if (!num.isEmpty()) {
            LogUtils.logon(Integer.parseInt(num));
        } else {
            LogUtils.logon();
        }
    }

    /**
     * Inicia host.
     *
     * @param id Identificador do Host.
     */
    private static void start(int id) {
        // Iniciad novo host com o identificador informado.
        AbstractHost a = new MultipleHost(id);
        // Insere no mapeamento de hosts.
        HOSTS.put(id, a);
        // Inicia thread de execução do host.
        new Thread(a).start();
    }

    /**
     *
     * Flag de simulação de RTT.
     *
     * @return <code>true</code> Ativa simulação <code>false</code> Do
     * contrário.
     */
    public static boolean isSimu() {
        return simu;
    }

}
