package br.edu.utfpr.host;

import br.edu.utfpr.protocol.MessageProtocol;
import br.edu.utfpr.service.ServiceLive;
import br.edu.utfpr.service.ServiceTime;
import br.edu.utfpr.T02;
import br.edu.utfpr.utils.LogUtils;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * Host de conexão multicast/unicast.
 *
 * @author Erik J ALmeida e Rodrigo G. Moroni.
 */
public abstract class AbstractHost implements Runnable {

    /**
     * identificador deste host.
     */
    protected int id;
    /**
     * Identificador do master.
     */
    private Integer master;
    /**
     * Endereço IPV4 do serviço Multicast.
     */
    private static final String MC = "228.5.6.10"; //endereço de Multicast
    /**
     * Porta padrão do serviço Multicast.
     */
    protected static final int MS_PORT = 8181;// porta para multicast    
    /**
     * Socket para o Multicast.
     */
    private MulticastSocket ms;
    /**
     * Socket para comunicação unicast.
     */
    private DatagramSocket socket;
    /**
     * Horario local deste host em milissegundos.
     */
    private long time; //hora do host
    /**
     * Define estado de exceução do host.
     */
    private volatile boolean running = false;
    /**
     * Ultima atualização de disponibilidade do master.
     */
    private long timeMaster;
    /**
     * Chave privada deste host.
     */
    private PrivateKey privateKey;
    /**
     * Chave publica deste host.
     */
    private PublicKey publicKey;

    /**
     * Mapeamento de identificação x chave pública.
     */
    private final Map<Integer, PublicKey> keys = new ConcurrentHashMap<>();
    /**
     * Mapeamento de votação: identificação x quantidade de votos.
     */
    private final Map<Integer, Integer> votes = new ConcurrentHashMap<>();

    /**
     * Construtor com item obrigátório necessário ao host.
     *
     * @param id identificador do host.
     */
    public AbstractHost(int id) {
        this.id = id;
        this.time = System.currentTimeMillis();
    }

    /**
     * Inicia serviço multicast/unicast e chaves publicas.
     */
    public void init() {

        try {

            // Inicia serviço unicast.
            socket = new DatagramSocket(getUnicastAddress(id));
            socket.setSoTimeout((int) ServiceLive.WAIT_MASTER);

            // Inicia serviço multicast.
            ms = new MulticastSocket(MS_PORT);
            ms.joinGroup(InetAddress.getByName(MC)); //entra no Multicast

            //criação das chaves
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024);

            KeyPair key = keyGen.generateKeyPair();
            privateKey = key.getPrivate();
            publicKey = key.getPublic();

            // Define que o serviço esta rodando.
            this.running = true;

        } catch (IOException | NoSuchAlgorithmException ex) {
            LogUtils.error(this, ex, "Error ao iniciar Host");
        }
    }

    /**
     * Fornece endereço do socket uniCast.
     *
     * @param id Identificador do host.
     * @return Endereço do socket uniCast.
     * @throws IOException Exceção de entrada/saida de stream/conexão.
     */
    protected abstract SocketAddress getUnicastAddress(int id) throws IOException;

    /**
     * Envia mensagem multicas
     *
     * @param type Tipo de mensagem.
     * @param content Conteúdo da mensagem
     * @throws IOException Exceção de entrada/saida de stream/conexão.
     */
    public void sendMulticast(MessageProtocol.TypeMessage type, Object content) throws IOException {

        // Obtém dados da mensagem.
        byte[] buffer = new MessageProtocol(this.id, type, content).serialize();
        // Envia dados da mensagem por multicast.
        ms.send(new DatagramPacket(buffer, buffer.length, InetAddress.getByName(MC), MS_PORT));

    }

    /**
     * Envia mensagem unicast.
     *
     * @param type Tipo de mensagem.
     * @param content Conteúdo da mensagem
     * @param id Identificador do host.
     * @throws IOException Exceção de entrada/saida de stream/conexão.
     */
    public void sendUnicast(MessageProtocol.TypeMessage type, Object content, int id) throws IOException {

        // Obtém dados da mensagem.
        byte[] buffer = new MessageProtocol(this.id, type, content).serialize();
        // Envia dados da mensagem por unicast.
        socket.send(new DatagramPacket(buffer, buffer.length, getUnicastAddress(id)));
    }

    @Override
    public void run() {

        // Inicializa serviços unicast/multicast e chaves.
        init();

        try {

            // Mensagem de entrada no grupo, informando chave pública.
            sendMulticast(MessageProtocol.TypeMessage.HELLO, publicKey);

            // Mensagem para ser usado na recepção.
            MessageProtocol msg = new MessageProtocol();
            // Aloca vetor com tamanhao maximo esperado.
            byte[] received = new byte[1024];
            // Anexação do vetor ao pacote UDP.
            DatagramPacket pkt = new DatagramPacket(received, received.length);

            // Inicia serviço de checagem de disponibilidade de master.
            new Thread(new ServiceLive(this)).start();
            // Inicia serviço de checagem de horários.
            new Thread(new ServiceTime(this)).start();

            /// Define se serviço esta rondando.
            while (running) {

                // recebe pacote.
                ms.receive(pkt);
                // cria mensagem a partir dos dados recebidos.
                msg.parse(pkt.getData());

                LogUtils.info(this, "[MULTICAST]: %s", msg.toString());
                // desconsidera mensagens próprias com exceção da mensagem de voto.
                if (msg.getOwner() == id && msg.getTypMessage() != MessageProtocol.TypeMessage.VOTE) {
                    // Evitar monopolio de fila do sistema.
                    Thread.sleep(100);
                    continue;
                }
                // trata as mensagens recebidas.
                threatMulticast(msg);
            }
            Thread.sleep(500);
        } catch (InterruptedException | IOException ex) {
            LogUtils.error(this, ex,"Erro na recepção de multicast");
        }
    }

    /**
     * trata das ações referentes as mensagens recebidas por multicast.
     * @param msg Mensagem recebidas via multicast.
     * @throws IOException Exceção de entrada/saida de stream/conexão
     */
    protected void threatMulticast(MessageProtocol msg) throws IOException {
        // Verifica tipo de mensagem.
        switch (msg.getTypMessage()) {
            case HELLO:
                // envia  mensagem com sua com sua chave.
                sendMulticast(MessageProtocol.TypeMessage.WORLD, publicKey);
            case WORLD:
                // recebe chaves publicas dos membros do grupo.
                keys.put(msg.getOwner(), (PublicKey) msg.getContent());
                break;
            case ELECTION:
                if (!keys.isEmpty()) {
                    // Chamada para eleição, envia o candidato com menor id.
                    Integer cand = keys.keySet().stream().min((n1, n2) -> n1 - n2).get();
                    // envia voto.
                    sendMulticast(MessageProtocol.TypeMessage.VOTE, cand);
                }
                break;
            case VOTE:
                // Registra e computa voto.
                Integer rg = (Integer) msg.getContent();
                Integer vote = votes.get(rg);
                votes.put(rg, null != vote ? vote + 1 : 1);
                break;
            case IM_LIVE:
                // Registra ultima disponibilidade recebida do master.
                master = msg.getOwner();
                timeMaster = System.currentTimeMillis();
                break;
            case DEATH:
                // Registra que um membro esta desconectado.
                keys.remove((int) msg.getContent());
                break;
        }
    }
    
    
    /**
     * Escolhe master para o grupo.
     */
    public void chooseMaster() {

        // Maior número de votos.
        int major = 0;
        Integer tempMaster = null;
        // percorre mapeamento computando votos.
        for (Map.Entry<Integer, Integer> entry : votes.entrySet()) {

            // se este host teve mais votos que o maior numero registrado.
            if (major < entry.getValue()) {
                // este será o master temporariamente.
                major = entry.getValue();
                tempMaster = entry.getKey();
            }
        }
        // Define novo Mestre.
        this.master = tempMaster;
        LogUtils.info(this, "[MASTER] For %d master is -> %d ", id, master);
        votes.clear();
    }

    /**
     * Mestre informa disponibilidade para demais membros.
     *
     * @throws IOException Exceção de entrada/saida de stram/conexão.
     */
    public void imAliveCambada() throws IOException {
        // Envia informa disponibilidade para demais menbros.
        sendMulticast(MessageProtocol.TypeMessage.IM_LIVE, 0);
    }

    /**
     * Remove membro do grupo.
     * @param id identificador do membro
     */
    public void removeMember(Integer id) {
        // remove chave publica do master.
        if( id != null){
            this.keys.remove(id);
            T02.HOSTS.remove(id);
        }
    }
    
    /**
     * Remove Mestre do grupo.
     */
    public void removeMaster(){
        removeMember(master);
        master = null;
    }

    /**
     * Obtém chave pública do master.
     *
     * @return Chave pública do master.
     */
    public PublicKey getMasterKey() {
        return this.keys.get(master);
    }

    /**
     * Verifica se este host é o master.
     *
     * @return <code>true</code> este host é o master <code>false</code> Do
     * Contrário.
     */
    public boolean imMaster() {
        return master != null ? master == id : false;
    }

    /**
     * Verifica se existe master ativo.
     *
     * @param timeout Timeout para disponibilidade do master.
     * @return <code>true</code> existe master ativo <code>false</code> Do
     * Contrário.
     */
    public boolean hasMaster(long timeout) {
        // caso id não esteja nulo e a disponibilidade não tenha excedido o timeout.
        return master != null && timeout > System.currentTimeMillis() - timeMaster;
    }

    /**
     * Execução do algoritmo de Berkeley.
     *
     * @param typeMessage Tipo de mensagem a ser enviada pelo master.
     * @throws IOException Exceção entrada/saída de stream/conexão.
     */
    public void berkeleyAlgorithm(MessageProtocol.TypeMessage typeMessage) throws IOException {

        // Soma dos tempos dos hosts no multicast (inclusive este).
        long sum = time;
        // Mapeamento identificador x tempo.
        Map<Integer, Long> asserts = new HashMap<>();
        // Mensagem para ser usado na recepção/envio.
        MessageProtocol msg = new MessageProtocol();
        // RTT processado de cada membro.
        long rtt;
        // Percorre lista de chaves.
        for (Integer key : keys.keySet()) {

            try {
                // Aloca vetor com tamanhao maximo esperado.
                byte[] bb = new byte[1024];
                // Anexação do vetor ao pacote UDP.
                DatagramPacket pkt = new DatagramPacket(bb, bb.length);
                rtt = System.currentTimeMillis();
                // Envia mensagem unicast solicitando horario.
                sendUnicast(typeMessage, 0, key);
                // Recebe pacote com horario do host.
                this.socket.receive(pkt);
                rtt = System.currentTimeMillis() - rtt;
                // cria mensagem a partir dos dados recebidos.
                msg.parse(pkt.getData());
                // Verifica se mensagem é do tipo tempo.
                if (msg.getTypMessage() == MessageProtocol.TypeMessage.TIME) {
                    // adiciona a soma e salva o horario do host.
                    rtt += (long) msg.getContent();
                    sum += rtt;
                    asserts.put(key, rtt);
                    LogUtils.info(this, "Clock %d: %d", key, rtt);
                }
            } catch (IOException e) {
                LogUtils.error(this, e, "Erro ao aguardar mensagem de membro.");
                sendMulticast(MessageProtocol.TypeMessage.DEATH, key);
                removeMember(key);
            }
            
        }

        // gera valor médio conforme tempos informados.
        final long timeFinal = Math.round(sum / (keys.size() + 1));
        LogUtils.info(this, "Clock avg: %d", timeFinal);

        // Percorre  ids e lista de tempos
        asserts.forEach((k, v) -> {
            try {
                // Cria mensagem de tempo com o ajuste necessário.
                MessageProtocol mmm = new MessageProtocol(id, MessageProtocol.TypeMessage.TIME, timeFinal - v);
                // Assina mensagem gerando hash
                byte[] hash = assignMessage(mmm);
                // Obtém dados desta mensagem.
                byte[] data = mmm.serialize();
                // Aloca buffer com o tamanho necessário para o comportar: o tamanho do hash, o hash e a mensagem.
                ByteBuffer buffer = ByteBuffer.allocate(2 + hash.length + data.length);

                // Insere tamanho do hash, hash e mensagem.
                buffer.putShort((short) hash.length);
                buffer.put(hash);
                buffer.put(data);

                // envia mensagem com conteudo criptografado.
                sendUnicast(MessageProtocol.TypeMessage.CRYPTO, buffer.array(), k);
            } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | SignatureException ex) {
                LogUtils.error(this, ex, "Envio de ajuste de tempo");
            }
        });
        
        //ajusta seu próprio tempo
        this.setTime(timeFinal);
    }

    /**
     * Asssinatura digital da mensagem.
     *
     * @param msg mensagem a ser assinada.
     * @return Dados da mensagem assinada como master.
     * @throws NoSuchAlgorithmException Exceção de algoritmo incorreto.
     * @throws SignatureException Exceção de assinatura invalida ou não
     * configurada corretamente.
     * @throws InvalidKeyException Exceção de chave informada seja invalida.
     */
    private byte[] assignMessage(MessageProtocol msg) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {

        // Instância assinatura eletrônica.
        Signature s = Signature.getInstance("SHA1WithRSA");
        // Insere chave privada do master.
        s.initSign(privateKey);
        // Insere dados da mensagem em claro.
        s.update(msg.serialize());
        // Forja assinatura eletrônica da mensagem.
        return s.sign();
    }

    /**
     * Define se processo esta rodando..
     *
     * @param running <code>true</code> processo esta rodando <code>false</code> do contrário.
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Obtém se processo esta rodando.
     *
     * @return <code>true</code> processo esta rodando <code>false</code> do contrário.
     */
    public boolean getRunning() {
        return this.running;
    }

    /**
     * Obtém identificador do master.
     *
     * @return Identificador do master.
     */
    public Integer getMaster() {
        return master;
    }

    /**
     * Obtém chave pública do master.
     *
     * @return Chave pública do master.
     */
    public PublicKey getPublicKey() {
        return this.publicKey;
    }

    /**
     * Obtém socket para comunicação unicast.
     *
     * @return Socket para comunicação unicast.
     */
    public DatagramSocket getSocket() {
        return socket;
    }

    /**
     * Obtém tempo de este host.
     *
     * @return Tempo de este host.
     */
    public long getTime() {
        return time;
    }

    /**
     * Define tempo de este host.
     *
     * @param time Tempo de este host.
     */
    public void setTime(long time) {
        this.time = time;
    }

}
