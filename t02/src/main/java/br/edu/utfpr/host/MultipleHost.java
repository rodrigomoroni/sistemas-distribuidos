package br.edu.utfpr.host;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import static br.edu.utfpr.host.AbstractHost.MS_PORT;
/**
 *
 * Host de conexão multicast/unicast com múltiplos hosts no IP.
 *
 * @author Erik J ALmeida e Rodrigo G. Moroni.
 */
public class MultipleHost extends AbstractHost{
    
    /**
     * Construtor com item obrigátório necessário ao host.
     *
     * @param id identificador do host.
     */
    public MultipleHost(int id) {
        super(id);
    }

    @Override
    protected SocketAddress getUnicastAddress(int id) throws  IOException{
        return  new InetSocketAddress(InetAddress.getByName("localhost"), MS_PORT+id);
    }

    
}
