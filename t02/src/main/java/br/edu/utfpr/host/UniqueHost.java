package br.edu.utfpr.host;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import static br.edu.utfpr.host.AbstractHost.MS_PORT;

/**
 *
 * Host de conexão multicast/unicast com apenas um host por IP.
 *
 * @author Erik J ALmeida e Rodrigo G. Moroni.
 */
public class UniqueHost extends AbstractHost {

    /**
     * Construtor com item obrigátório necessário ao host.
     *
     * @param id identificador do host.
     */
    public UniqueHost(int id) {
        super(id);
    }

    @Override
    protected SocketAddress getUnicastAddress(int id) throws IOException {

        // Transforma o identificador do host no endereço ip do host.
        InetAddress addr = InetAddress.getByAddress(ByteBuffer.allocate(4).putInt(id).array());
        // Retorna endereço do socket.
        return new InetSocketAddress(addr, MS_PORT + 1);
    }

}
