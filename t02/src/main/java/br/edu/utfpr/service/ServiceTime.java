package br.edu.utfpr.service;

import br.edu.utfpr.protocol.MessageProtocol;
import br.edu.utfpr.T02;
import br.edu.utfpr.host.AbstractHost;
import br.edu.utfpr.utils.LogUtils;
import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

/**
 * 
 * Define rotina de verificação da rotina de ajuste dos relógios(Algoritmo de Berkley)
 * 
 * @see AbstractHost
 * 
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public class ServiceTime implements IService {

    /** Host de conexões multicast/unicast. */
    private final AbstractHost host;
    /** Instância de mensagem para recepção de unicast. */
    private final MessageProtocol msg;
    /** Define tempo t2 para correção do horario dos relógios. */
    private final long ASSERT_CLOCK = 300L;

    /**
    * Construtor padrão com {@link AbstractHost} Obrigátório.
    * @param host Host de conexões multicast/unicast.
    */
    public ServiceTime(AbstractHost host) {
        this.host = host;
        this.msg = new MessageProtocol();
    }

    @Override
    public void run() {

        // Aloca vetor com tamanhao maximo esperado.
        byte[] b = new byte[1024];
        // Anexação do vetor ao pacote UDP.
        DatagramPacket pkt = new DatagramPacket(b, b.length);

        // Enquanto host estiver disponivel.
        while (host.getRunning()) {

            try {
                // verifica qual rotina deve ser cumprida.
                if (host.imMaster()) {
                    routineMaster();
                // Verifica se master esta disponivel.
                } else if(host.hasMaster(ServiceLive.WAIT_MASTER)){
                    // Aguada comunicação unicast com master.
                    host.getSocket().receive(pkt);
                    // cria mensagem a partir dos dados recebidos.
                    this.msg.parse(pkt.getData());
                    // Executa rotina não-mestre.
                    routineNonMaster();
                } else{
                    // Caso não haja ação a executar aguarda.
                    Thread.sleep(ASSERT_CLOCK);
                }

            } catch (IOException | InterruptedException ex) {
                LogUtils.error(this, ex, "Erro na rotina de tempo.");
            }

        }

    }

    @Override
    public void routineMaster() throws IOException,InterruptedException {
       
        // Aguarda tempo defin para correção dos horários.
        Thread.sleep(ASSERT_CLOCK);
        // Executa algortimo de berkeley.
        host.berkeleyAlgorithm(MessageProtocol.TypeMessage.GET_CLOCK);
    }

    @Override
    public void routineNonMaster() throws IOException,InterruptedException{

        try {
            
            // Verifica tipo de mensagem unicast recebida do master.
            LogUtils.info(this, "[UNICAST] %s", msg.toString());
            switch (msg.getTypMessage()) {
                case GET_CLOCK:
                    // Gera erro para simular RTT.
                    if(T02.isSimu()){
                        host.setTime(((long) (Math.random() * 10L))+host.getTime());
                    }
                    host.sendUnicast(MessageProtocol.TypeMessage.TIME, host.getTime(), host.getMaster());
                    break;
                case CRYPTO:
                    // Descriptografa mensagem vinda do master.
                    this.decrypt(msg);
                    break;
            }
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            LogUtils.error(this, e, "Erro recepção de Unicast/ Criptografia.");
        }

    }
    
    
    /**
     * Decodifica a mensagem de ajuste de horario recebida do mestre.
     * @param msg mensagem com assinatura eletrônica do mestre.
     * @throws InvalidKeyException Exceção de chave informada seja invalida.
     * @throws NoSuchAlgorithmException Exceção de algoritmo incorreto.
     * @throws SignatureException Exceção de assinatura invalida ou não configurada corretamente.
     */
    private void decrypt(MessageProtocol msg) throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {

        // Insere conteúdo da mensagem no buffer.
        ByteBuffer buffer = ByteBuffer.wrap((byte[]) msg.getContent());
        // Obtém dados da mensagem assinada.
        byte[] hash = MessageProtocol.getBytes(buffer);
        // Aloca vetor para dados da mensagem em claro. 
        byte[] data = new byte[buffer.remaining()];
        // Obtém dados da mensagem em claro.
        buffer.get(data,0,data.length);
        
        // Instância assinatura eletrônica.
        Signature s = Signature.getInstance("SHA1WithRSA");
        // Insere chave pública do master.
        s.initVerify(host.getMasterKey());
        // Insere dados da mensagem em claro.
        s.update(data);
        // Verifica se mensagem é realmente do master.
        if (s.verify(hash)) {
            // caso seja instância mensagem de tempo e ajusta relógio.
            msg.parse(data);
            long old = host.getTime();
            host.setTime(host.getTime()+(long) msg.getContent());
            LogUtils.info(this,"[INFO] Adjust: %d + %d = %d",old,(long) msg.getContent(),host.getTime());
        }
    }

}
