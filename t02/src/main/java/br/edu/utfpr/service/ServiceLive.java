package br.edu.utfpr.service;

import br.edu.utfpr.protocol.MessageProtocol;
import br.edu.utfpr.host.AbstractHost;
import br.edu.utfpr.utils.LogUtils;
import java.io.IOException;

/**
 * Verificar se o Master está vivo...se o Host for o Master, vai enviar as
 * mensagens de que está vivo, se o Host for Slave vai verificar o recebimento
 * das mensagens.
 *
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public class ServiceLive implements IService {

    /**
     * Host de conexões multicast/unicast.
     */
    private final AbstractHost host;
    /**
     * Tempo de tolerância para disponibilidade do master.
     */
    public static final long WAIT_MASTER = 500L;
    /**
     * Tempo de t1 para informação de disponilidade do master.
     */
    private final long IM_LIVE = 250L;

    /**
     * Construtor padrão com {@link AbstractHost} Obrigátório.
     *
     * @param host Host de conexões multicast/unicast.
     */
    public ServiceLive(AbstractHost host) {
        this.host = host;
    }

    @Override
    public void run() {
        
        // Enquanto host estiver disponivel.
        while (host.getRunning()) {
            try {

                // verifica qual rotina deve ser cumprida.
                if (host.imMaster()) {
                    // caso master...
                    routineMaster();
                } else {
                    /// do contrário.
                    routineNonMaster();
                }

            } catch (IOException | InterruptedException ex) {
                LogUtils.error(this, ex, "Erro na rotina de dispobilidade de mestre.");
            }
        }
    }

    @Override
    public void routineNonMaster() throws InterruptedException, IOException {

        // Aguarda tempo de tolerância de disponibilidade do master.
        Thread.sleep(WAIT_MASTER);

        // Se não houver disponibilidade do master.
        if (!host.hasMaster(WAIT_MASTER)) {
            host.removeMaster();
            // Envia mensagem para nova eleição do mestre.
            host.sendMulticast(MessageProtocol.TypeMessage.ELECTION, 0);
            // Aguarda recepção dos votos.
            Thread.sleep(WAIT_MASTER);
            // Escolhe novo mestre.
            host.chooseMaster();
        }

    }

    @Override
    public void routineMaster() throws InterruptedException, IOException {

        // Tempo de t1 para anunciação do mestre.
        Thread.sleep(IM_LIVE);
        // Mestre envia resposta so grupo
        host.imAliveCambada();

    }
}
