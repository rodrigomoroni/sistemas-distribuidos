package br.edu.utfpr.service;

import java.io.IOException;

/**
 * 
 * Prestação de serviços ao host.
 * 
 * @author Erik J Almeida e Rodrigo G. Moroni.
 */
public interface IService extends Runnable{
    
    /**
     * Define Rotina executada pelo master.
     * @throws IOException Exceção de Entrada/saída de stream.
     * @throws InterruptedException Exceção de interrupação de Thread.
     */
    void routineMaster  () throws IOException,InterruptedException;
    
    /**
     * Define Rotina executada pelos membros não-master.
     * @throws IOException Exceção de Entrada/saída de stream.
     * @throws InterruptedException Exceção de interrupação de Thread.
     */
    void routineNonMaster() throws IOException,InterruptedException;
    
}
